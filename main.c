#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

extern int32_t suma(int32_t, int32_t);
extern void    z_c();

int main(void)
{
int32_t s = suma(1, 2);

 z_c();
 printf("Wynik %d\n", s);

return 0;
}
