CC = gcc
CC = clang

#Problemy przy prawidłowym wywoływaniu funkcji rust z programu w C

main: main.o libex.a
	$(CC) -L. -o main main.o -lex -lpthread -lgcc_s -ldl

main.o: main.c
	$(CC) -c main.c

sum.o: sum.rs Makefile
	rustc --crate-type=staticlib --emit=obj sum.rs

libsum.a: sum.rs Makefile
	rustc --crate-type=staticlib sum.rs

libex.a: sum.rs
	rustc --crate-type staticlib --crate-name ex sum.rs


mem: ./main
	valgrind -v --leak-check=full --show-leak-kinds=all --track-origins=yes --tool=memcheck ./main

clean:
	rm -f ./*.a ./*.o ./main
