#![crate_type = "dylib"]
#![crate_type = "staticlib"]

#[no_mangle]
pub extern "C" fn suma(a: i32, b: i32) -> i32
{
  a + b
}

#[no_mangle]
pub extern "C" fn z_c()
{
let result = std::panic::catch_unwind(|| {
        println!("Po prostu wypisz napis!"); });
 if result.is_err()
  { eprintln!("Łojej: rust panikuje"); }
}
